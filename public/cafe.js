var cafetiere, tasse, liquide, colonne;
var remplissage = false;
var posee = false;
var tasse_niveau = 0;
var cafetiereX, cafetiereY;
var time = 0;
var son;

function setup() {

  createCanvas(1000, 1000);
  console.log("Setup!");

  cafetiere = loadImage("data/cafetiere.png");
  tasse = loadImage("data/tasse.png");
  liquide = [];
  for (var i = 0; i <= 5; i++)
  {
    liquide.push(loadImage("data/liquide_" + i + ".png"));
  }
  colonne = loadImage("data/colonne_liquide.png");
  son = loadSound("data/bruit_cafetiere.ogg");  
  setPosee(false);
}

function setPosee(_posee)
{
  posee = _posee;
  
  if (!posee)
  {
    noCursor();
  }
  else
  {
    cursor();
  }
}

function setRemplissage(_remplissage)
{
   remplissage = _remplissage;
   
   if (remplissage)
   {
      son.play(); 
   }
   else
     son.stop();
}

function draw() {
  background(255);
  
  cafetiereX = width / 2 - cafetiere.width / 2;
  cafetiereY = height / 2 - cafetiere.height / 2;

  image(cafetiere, cafetiereX, cafetiereY);

  var tasseX, tasseY;
  if (!posee)
  {
    tasseX = mouseX - tasse.width/2;
    tasseY = mouseY - tasse.height/2;
  } else
  {
    tasseX = cafetiereX + 78;
    tasseY = cafetiereY + 217;
  }

  image(tasse, tasseX, tasseY);
  image(liquide[tasse_niveau], tasseX, tasseY);

  if (remplissage)
  {
    image(colonne, cafetiereX + 127, cafetiereY + 191);
    if (millis() - time >= 300)
    {
      tasse_niveau++;
      time = millis();

      if (tasse_niveau === 5)
      {
        setRemplissage(false);
      }
    }
  }
}

function toucheCafetiere(x, y)
{
  return x >= cafetiereX && x < cafetiereX + cafetiere.width
    && y >= cafetiereY && y < cafetiereY + cafetiere.height;
}

function mouseClicked()
{
  if (!remplissage && toucheCafetiere(mouseX, mouseY))
  {
    if (!posee && tasse_niveau === 0)
    {
      setRemplissage(true);
      setPosee(true);
      time = millis();
    } else if (posee && tasse_niveau === 5)
    {
      setPosee(false);
    }
  }
}